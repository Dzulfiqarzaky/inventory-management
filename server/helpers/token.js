const jwt = require("jsonwebtoken");

const generateToken = (payload) => {
    return jwt.sign(payload, process.env.SECRET_TOKEN);
};

const verifyToken = (token) => {
    return jwt.verify(token, process.env.SECRET_TOKEN);
};

module.exports = {
    generateToken,
    verifyToken,
};
