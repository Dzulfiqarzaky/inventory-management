const { ProductModel } = require("../models/productModel");

class ProductController {
    static async getAllProduct(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await ProductModel.getAllProduct({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getProductById(req, res, next) {
        try {
            const productId = req.params.id;
            const data = await ProductModel.getProductById(productId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createProduct(req, res, next) {
        try {
            const productData = req.body;
            const data = await ProductModel.createProduct(productData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateProduct(req, res, next) {
        try {
            const productId = req.params.id;
            const productData = req.body;
            const data = await ProductModel.updateProductById(
                productId,
                productData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteProduct(req, res, next) {
        try {
            const productId = req.params.id;
            const data = await ProductModel.deleteProductById(productId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = ProductController;
