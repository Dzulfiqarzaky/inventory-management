const { CustomerModel } = require("../models/customerModel");

class CustomerController {
    static async getAllCustomer(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await CustomerModel.getAllCustomer({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getCustomerById(req, res, next) {
        try {
            const customerId = req.params.id;
            const data = await CustomerModel.getCustomerById(customerId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createCustomer(req, res, next) {
        try {
            const customerData = req.body;
            const data = await CustomerModel.createCustomer(customerData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateCustomer(req, res, next) {
        try {
            const customerId = req.params.id;
            const customerData = req.body;
            const data = await CustomerModel.updateCustomerById(
                customerId,
                customerData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteCustomer(req, res, next) {
        try {
            const customerId = req.params.id;
            const data = await CustomerModel.deleteCustomerById(customerId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = CustomerController;
