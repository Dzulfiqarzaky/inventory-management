const ProductionModel = require("../models/productionModel");

class ProductionController {
    static async getAllProduction(req, res, next) {
        try {
            const { search, page, limit } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await ProductionModel.getAllProduction(
                searchText,
                pageNumber,
                limitPerPage
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getProductionById(req, res, next) {
        try {
            const productionId = req.params.id;
            const data = await ProductionModel.getProductionById(productionId);
            res.status(200).json(data);
        } catch (error) {
            next(error);
        }
    }
    static async createProduction(req, res, next) {
        try {
            const productionData = req.body;
            const data = await ProductionModel.createProduction(productionData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateProduction(req, res, next) {
        try {
            const productionId = req.params.id;
            const productionData = req.body;
            const data = await ProductionModel.updateProductionById(
                productionId,
                productionData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteProduction(req, res, next) {
        try {
            const productionId = req.params.id;
            const data = await ProductionModel.deleteProductionById(
                productionId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = ProductionController;
