const { SupplierModel } = require("../models/supplierModel");

class SupplierController {
    static async getAllSupplier(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await SupplierModel.getAllSupplier({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getSupplierById(req, res, next) {
        try {
            const supplierId = req.params.id;
            const data = await SupplierModel.getSupplierById(supplierId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createSupplier(req, res, next) {
        try {
            const supplierData = req.body;
            const data = await SupplierModel.createSupplier(supplierData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateSupplier(req, res, next) {
        try {
            const supplierId = req.params.id;
            const supplierData = req.body;
            const data = await SupplierModel.updateSupplierById(
                supplierId,
                supplierData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteSupplier(req, res, next) {
        try {
            const supplierId = req.params.id;
            const data = await SupplierModel.deleteSupplierById(supplierId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = SupplierController;
