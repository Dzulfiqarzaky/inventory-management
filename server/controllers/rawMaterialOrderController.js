const RawMaterialOrderModel = require("../models/rawMaterialOrderModel");

class RawMaterialOrderController {
    static async getAllRawMaterialOrder(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await RawMaterialOrderModel.getAllRawMaterialOrder({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getRawMaterialOrderById(req, res, next) {
        try {
            const rawMaterialOrderId = req.params.id;
            const data = await RawMaterialOrderModel.getRawMaterialOrderById(
                rawMaterialOrderId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createRawMaterialOrder(req, res, next) {
        try {
            const rawMaterialOrderData = req.body;
            const data = await RawMaterialOrderModel.createRawMaterialOrder(
                rawMaterialOrderData
            );

            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateRawMaterialOrder(req, res, next) {
        try {
            const rawMaterialOrderId = req.params.id;
            const rawMaterialOrderData = req.body;
            const data = await RawMaterialOrderModel.updateRawMaterialOrderById(
                rawMaterialOrderId,
                rawMaterialOrderData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteRawMaterialOrder(req, res, next) {
        try {
            const rawMaterialOrderId = req.params.id;
            const data = await RawMaterialOrderModel.deleteRawMaterialOrderById(
                rawMaterialOrderId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = RawMaterialOrderController;
