const InvoiceModel = require("../models/invoiceModel");

class InvoiceController {
    static async getAllInvoice(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await InvoiceModel.getAllInvoice({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getInvoiceById(req, res, next) {
        try {
            const invoiceId = req.params.id;
            const data = await InvoiceModel.getInvoiceById(invoiceId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createInvoice(req, res, next) {
        try {
            const invoiceData = req.body;
            const data = await InvoiceModel.createInvoice(invoiceData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateInvoice(req, res, next) {
        try {
            const invoiceId = req.params.id;
            const invoiceData = req.body;
            const data = await InvoiceModel.updateInvoiceById(
                invoiceId,
                invoiceData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteInvoice(req, res, next) {
        try {
            const invoiceId = req.params.id;
            const data = await InvoiceModel.deleteInvoiceById(invoiceId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = InvoiceController;
