const RawMaterialUseModel = require("../models/rawMaterialUseModel");

class RawMaterialUseController {
    static async getAllRawMaterialUse(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await RawMaterialUseModel.getAllRawMaterialUse({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getRawMaterialUseById(req, res, next) {
        try {
            const rawMaterialUseId = req.params.id;
            const data = await RawMaterialUseModel.getRawMaterialUseById(
                rawMaterialUseId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createRawMaterialUse(req, res, next) {
        try {
            const rawMaterialUseData = req.body;
            const data = await RawMaterialUseModel.createRawMaterialUse(
                rawMaterialUseData
            );
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateRawMaterialUse(req, res, next) {
        try {
            const rawMaterialUseId = req.params.id;
            const rawMaterialUseData = req.body;
            const data = await RawMaterialUseModel.updateRawMaterialUseById(
                rawMaterialUseId,
                rawMaterialUseData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteRawMaterialUse(req, res, next) {
        try {
            const rawMaterialUseId = req.params.id;
            const data = await RawMaterialUseModel.deleteRawMaterialUseById(
                rawMaterialUseId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = RawMaterialUseController;
