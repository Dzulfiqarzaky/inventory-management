const BillModel = require("../models/billModel");

class BillController {
    static async getAllBill(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await BillModel.getAllBill({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getBillById(req, res, next) {
        try {
            const billId = req.params.id;
            const data = await BillModel.getBillById(billId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createBill(req, res, next) {
        try {
            const billData = req.body;
            const data = await BillModel.createBill(billData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateBill(req, res, next) {
        try {
            const billId = req.params.id;
            const billData = req.body;
            const data = await BillModel.updateBillById(
                billId,
                billData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteBill(req, res, next) {
        try {
            const billId = req.params.id;
            const data = await BillModel.deleteBillById(billId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = BillController;
