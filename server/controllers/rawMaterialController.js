const { RawMaterialModel } = require("../models/rawMaterialModel");

class RawMaterialController {
    static async getAllRawMaterial(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await RawMaterialModel.getAllRawMaterial({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getRawMaterialById(req, res, next) {
        try {
            const rawMaterialId = req.params.id;
            const data = await RawMaterialModel.getRawMaterialById(
                rawMaterialId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createRawMaterial(req, res, next) {
        try {
            const rawMaterialData = req.body;
            const data = await RawMaterialModel.createRawMaterial(
                rawMaterialData
            );
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateRawMaterial(req, res, next) {
        try {
            const rawMaterialId = req.params.id;
            const rawMaterialData = req.body;
            const data = await RawMaterialModel.updateRawMaterialById(
                rawMaterialId,
                rawMaterialData
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteRawMaterial(req, res, next) {
        try {
            const rawMaterialId = req.params.id;
            const data = await RawMaterialModel.deleteRawMaterialById(
                rawMaterialId
            );
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = RawMaterialController;
