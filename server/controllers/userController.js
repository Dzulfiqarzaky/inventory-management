const { UserModel } = require("../models/userModel");

class UserController {
    static async getAllUser(req, res, next) {
        try {
            const { search, page, limit, sortBy, sortOrder } = req.query;
            const searchText = search || "";
            const pageNumber = parseInt(page) || 1;
            const limitPerPage = parseInt(limit) || 10;

            const data = await UserModel.getAllUser({
                searchText,
                pageNumber,
                limitPerPage,
                sortBy,
                sortOrder,
            });
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async getUserById(req, res, next) {
        try {
            const userId = req.params.id;
            const data = await UserModel.getUserById(userId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async createUser(req, res, next) {
        try {
            const userData = req.body;
            const data = await UserModel.createUser(userData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async updateUser(req, res, next) {
        try {
            const userId = req.params.id;
            const userData = req.body;
            const data = await UserModel.updateUserById(userId, userData);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
    static async deleteUser(req, res, next) {
        try {
            const userId = req.params.id;
            const data = await UserModel.deleteUserById(userId);
            res.status(200).json({ data });
        } catch (error) {
            next(error);
        }
    }
}
module.exports = UserController;
