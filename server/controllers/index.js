// const { comparePassword } = require('../helper/bcrypt');
// const { generateToken } = require('../helper/jwt');

const { UserModel } = require("../models/userModel");

class IndexController {
    static async login(req, res, next) {
        try {
            const userData = req.body;
            const data = await UserModel.login(userData);
            res.status(201).json({ data });
        } catch (err) {
            next(err);
        }
    }

    static async logout(req, res, next) {
        try {
            const userData = req.body;
            const data = await UserModel.login(userData);
            res.status(201).json({ data });
        } catch (err) {
            next(err);
        }
    }

    static async register(req, res, next) {
        try {
            const userData = req.body;
            const data = await UserModel.createUser(userData);
            res.status(201).json({ data });
        } catch (error) {
            next(error);
        }
    }
}

module.exports = IndexController;
