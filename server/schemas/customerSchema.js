const mongoose = require("mongoose");

const customerSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "Enter customer name."],
            unique: [true, "Name already taken."],
        },
        subCustomer: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Customer",
            },
        ],
        address: { type: String, required: true },
        isSubCustomer: { type: Boolean, default: false },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = customerSchema;
