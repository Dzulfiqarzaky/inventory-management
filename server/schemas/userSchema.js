const mongoose = require("mongoose");
const validator = require("validator");

const userSchema = mongoose.Schema(
    {
        username: {
            type: String,
            required: [true, "Enter a username."],
            unique: [true, "Username already taken."],
            lowercase: true,
            validate: [
                validator.isAlphanumeric,
                "Username may only have letters and numbers.",
            ],
        },
        password: {
            type: String,
            required: [true, "Password is required."],
            minlength: [6, "Password minimum requirements is 6 characters."],
        },
        role: {
            type: String,
            default: "user",
            enum: ["superadmin", "admin", "user"],
        },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = userSchema;
