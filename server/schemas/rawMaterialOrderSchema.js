const mongoose = require("mongoose");

const rawMaterialOrderSchema = new mongoose.Schema(
    {
        invoiceNumber: { type: String, unique: true },
        invoiceDate: { type: Date, required: true },
        rawMaterialBuyDate: { type: Date, required: true },
        supplier: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Supplier",
            required: true,
        },
        note: { type: String },
        rawMaterialItem: [
            {
                rawMaterial: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "RawMaterial",
                    required: true,
                },
                qty: { type: String, required: true },
                price: { type: String, required: true },
            },
        ],
        totalPrice: { type: String, required: true },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = rawMaterialOrderSchema;
