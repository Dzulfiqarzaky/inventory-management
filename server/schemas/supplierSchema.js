const mongoose = require("mongoose");

const supplierSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = supplierSchema;
