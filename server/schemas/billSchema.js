const mongoose = require("mongoose");

const billSchema = new mongoose.Schema(
    {
        billNumber: { type: String, unique: true },
        customer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Customer",
            required: true,
        },
        additionalCost: [
            {
                description: { type: String, required: true },
                price: { type: String, required: true },
            },
        ],
        total: { type: String, required: true },
        status: {
            type: String,
            enum: ["Created", "Printed", "Billed", "Paid"],
            default: "Created",
        },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = billSchema;
