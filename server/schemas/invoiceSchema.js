const mongoose = require("mongoose");

const invoiceSchema = new mongoose.Schema(
    {
        billNumber: { type: String, unique: true },
        sellingDate: { type: Date, required: true },
        customer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Customer",
            required: true,
        },
        subCustomer: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Customer.subCustomer",
                default: null,
            },
        ],
        productItems: [
            {
                product: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "Product",
                    required: true,
                },
                qty: { type: String, required: true },
                price: { type: String, required: true },
            },
        ],
        via: { type: String },
        additionalCost: [
            {
                description: { type: String, required: true },
                price: { type: String, required: true },
            },
        ],
        note: { type: String },
        total: { type: String, required: true },
        status: {
            type: String,
            enum: ["Created", "Printed", "Billed", "Paid"],
            default: "Created",
        },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = invoiceSchema;
