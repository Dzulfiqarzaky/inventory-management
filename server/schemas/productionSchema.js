const mongoose = require("mongoose");

const productionSchema = new mongoose.Schema(
    {
        productionDate: { type: Date, required: true },
        note: { type: String },
        productItems: [
            {
                product: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "Product",
                    required: true,
                },
                qty: { type: String, required: true },
                uom: { type: String, required: true },
            },
        ],
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = productionSchema;
