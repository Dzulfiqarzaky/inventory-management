const mongoose = require("mongoose");

const rawMaterialSchema = new mongoose.Schema(
    {
        SKU: { type: String, required: true, unique: true },
        name: { type: String, required: true, unique: true },
        unit: { type: String, required: true },
        qty: { type: String, required: true },
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = rawMaterialSchema;
