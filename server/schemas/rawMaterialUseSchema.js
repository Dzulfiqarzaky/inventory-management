const mongoose = require("mongoose");

const rawMaterialUseSchema = new mongoose.Schema(
    {
        rawMaterialUsedDate: { type: Date, required: true },
        note: { type: String },
        rawMaterialItem: [
            {
                rawMaterial: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "RawMaterial",
                    required: true,
                },
                // unit: { type: String, required: true },
                qty: { type: String, required: true },
            },
        ],
    },
    {
        timestamps: true,
        validateBeforeSave: true,
    }
);

module.exports = rawMaterialUseSchema;
