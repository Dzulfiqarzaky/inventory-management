const handleValidationError = (err) => {
    const errors = Object.values(err.errors).map((el) => el.message);
    return errors.join(",");
};

const handleDuplicateKeyError = (err) => {
    const field = Object.keys(err.keyValue);
    return `${field} already exists.`;
};

const errorHandler = (err, req, res, next) => {
    console.log(err);
    let message = "Internal Server Error";
    let status = 500;
    switch (err.name) {
        case "ValidationError":
            message = handleValidationError(err);
            status = 400;
            break;
        case "JsonWebTokenError":
            message = "Invalid token!";
            status = 401;
            break;
        case "Forbidden":
        case "Login Required":
        case "Not Authorized":
            message = err.name;
            status = 403;
            break;
        case "Invalid Username or Password":
        case "User Not Found":
        case "Customer Not Found":
        case "Invoice Not Found":
        case "Production Not Found":
        case "Product Not Found":
        case "RawMaterial Not Found":
        case "Order of Raw Material Not Found":
        case "Used Raw Material Not Found":
            message = err.name;
            status = 404;
            break;
        case "CastError":
            message = `Invalid ${err.path}: ${err.value}`;
            status = 404;
            break;
        case "Duplicate Sub-customer.":
        case "Customer with Sub-customer cannot be added.":
        case "This Customer is a Sub-customer.":
        case "Sub-customer already a sub-customer of another customer.":
        case "Sub-customer Not Found":
        case "Customer name already taken.":
            message = err.name;
            status = 409;
            break;
        case "Transaction Failed.":
            message = err.name;
            status = 410;
            break;
    }
    if (err.code && err.code === 11000) {
        message = handleDuplicateKeyError(err);
        status = 409;
    }
    const errorResponse = {
        error: {
            message,
            status,
        },
    };
    res.status(status).json(errorResponse);
};

module.exports = errorHandler;
