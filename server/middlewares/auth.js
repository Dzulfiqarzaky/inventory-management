const { verifyToken } = require("../helpers/token");
const { User } = require("../models/userModel");

const authentication = async (req, res, next) => {
    try {
        const { access_token } = req.headers;
        if (!access_token) throw { name: "Login Required" };
        const { username, role } = verifyToken(access_token.split(" ")[1]);
        const user = await User.findOne({
            username,
            role,
        });
        if (!user) throw { name: "Not Authorized" };
        req.loginInfo = {
            id: user.id,
            role: user.role,
        };
        next();
    } catch (error) {
        next(error);
    }
};

const superadminAuth = async (req, res, next) => {
    try {
        const { role } = req.loginInfo;
        if (role !== "superadmin") {
            throw { name: "Forbidden" };
        }
        next();
    } catch (error) {
        next(error);
    }
};

const adminAuth = async (req, res, next) => {
    try {
        const { role } = req.loginInfo;
        if (role !== "admin" && role !== "superadmin") {
            throw { name: "Forbidden" };
        }
        next();
    } catch (error) {
        next(error);
    }
};

module.exports = {
    authentication,
    superadminAuth,
    adminAuth,
};
