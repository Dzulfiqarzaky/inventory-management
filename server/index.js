const dotenv = require("dotenv").config();

const cors = require("cors");
const express = require("express");
const routers = require("./routes/");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const errorHandler = require("./middlewares/errorHandler");

const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());
app.use(routers);
app.use(errorHandler);

// connect mongodb and start server
mongoose
    .connect(process.env.MONGO_URI)
    .then(() => {
        app.listen(PORT, () => {
            console.log(`server running on port ${PORT}`);
        });
    })
    .catch((err) => console.log(err));
