const express = require("express");
const RawMaterialController = require("../controllers/rawMaterialController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");
const rawMaterialOrderRouter = require("./rawMaterialOrder");
const rawMaterialUseRouter = require("./rawMaterialUse");
const router = express.Router();

// rawMaterial routes
router.get("/", RawMaterialController.getAllRawMaterial);
router.post("/", adminAuth, RawMaterialController.createRawMaterial);
router.use("/order", rawMaterialOrderRouter);
router.use("/use", rawMaterialUseRouter);
router.get("/:id", RawMaterialController.getRawMaterialById);
router.put("/:id", superadminAuth, RawMaterialController.updateRawMaterial);
router.delete("/:id", superadminAuth, RawMaterialController.deleteRawMaterial);

module.exports = router;
