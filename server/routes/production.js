const express = require("express");
const ProductionController = require("../controllers/productionController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// production routes
router.get("/", ProductionController.getAllProduction);
router.post("/", adminAuth, ProductionController.createProduction);
router.get("/:id", ProductionController.getProductionById);
router.put("/:id", superadminAuth, ProductionController.updateProduction);
router.delete("/:id", superadminAuth, ProductionController.deleteProduction);

module.exports = router;
