const express = require("express");
const CustomerController = require("../controllers/customerController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// CRUD
router.get("/", CustomerController.getAllCustomer);
router.post("/", adminAuth, CustomerController.createCustomer);
router.get("/:id", CustomerController.getCustomerById);
router.put("/:id", superadminAuth, CustomerController.updateCustomer);
router.delete("/:id", superadminAuth, CustomerController.deleteCustomer);

module.exports = router;
