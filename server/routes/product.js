const express = require("express");
const ProductController = require("../controllers/productController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");
const productionRouter = require("./production");

const router = express.Router();

// product routes
router.get("/", ProductController.getAllProduct);
router.post("/", adminAuth, ProductController.createProduct);
router.use("/production", productionRouter);
router.get("/:id", ProductController.getProductById);
router.put("/:id", superadminAuth, ProductController.updateProduct);
router.delete("/:id", superadminAuth, ProductController.deleteProduct);

module.exports = router;
