const express = require("express");
const IndexController = require("../controllers");
const userRouter = require("./users");
const productRouter = require("./product");
const supplierRouter = require("./supplier");
const customerRouter = require("./customer");
const rawMaterialRouter = require("./rawMaterial");
const invoiceRouter = require("./invoice");
const billRouter = require("./bill");
const { authentication } = require("../middlewares/auth");
// const {adminAuth} = require('../middlewares/adminAuth');

const router = express.Router();

// login routes
router.post("/login", IndexController.login);
router.post("/logout", IndexController.logout);
router.post("/register", IndexController.register);

// user routes
router.use("/user", authentication, userRouter);
router.use("/product", authentication, productRouter);
router.use("/supplier", authentication, supplierRouter);
router.use("/customer", authentication, customerRouter);
router.use("/invoice", authentication, invoiceRouter);
router.use("/bill", authentication, billRouter);
router.use("/raw-material", authentication, rawMaterialRouter);

module.exports = router;
