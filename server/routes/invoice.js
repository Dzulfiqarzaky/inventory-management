const express = require("express");
const InvoiceController = require("../controllers/invoiceController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// invoice routes
router.get("/", InvoiceController.getAllInvoice);
router.post("/", adminAuth, InvoiceController.createInvoice);
router.get("/:id", InvoiceController.getInvoiceById);
router.put("/:id", superadminAuth, InvoiceController.updateInvoice);
router.delete("/:id", superadminAuth, InvoiceController.deleteInvoice);

module.exports = router;
