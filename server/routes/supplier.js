const express = require("express");
const SupplierController = require("../controllers/supplierController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// supplier routes
router.get("/", SupplierController.getAllSupplier);
router.post("/", adminAuth, SupplierController.createSupplier);
router.get("/:id", SupplierController.getSupplierById);
router.put("/:id", superadminAuth, SupplierController.updateSupplier);
router.delete("/:id", superadminAuth, SupplierController.deleteSupplier);

module.exports = router;
