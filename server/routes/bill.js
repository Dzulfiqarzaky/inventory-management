const express = require("express");
const BillController = require("../controllers/billController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// bill routes
router.get("/", BillController.getAllBill);
router.post("/", adminAuth, BillController.createBill);
router.get("/:id", BillController.getBillById);
router.put("/:id", superadminAuth, BillController.updateBill);
router.delete("/:id", superadminAuth, BillController.deleteBill);

module.exports = router;
