const express = require("express");
const RawMaterialOrderController = require("../controllers/rawMaterialOrderController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// rawMaterialOrder routes
router.get("/", RawMaterialOrderController.getAllRawMaterialOrder);
router.post("/", adminAuth, RawMaterialOrderController.createRawMaterialOrder);
router.get("/:id", RawMaterialOrderController.getRawMaterialOrderById);
router.put(
    "/:id",
    superadminAuth,
    RawMaterialOrderController.updateRawMaterialOrder
);
router.delete(
    "/:id",
    superadminAuth,
    RawMaterialOrderController.deleteRawMaterialOrder
);

module.exports = router;
