const express = require("express");
const RawMaterialUseController = require("../controllers/rawMaterialUseController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// rawMaterialUse routes
router.get("/", RawMaterialUseController.getAllRawMaterialUse);
router.post("/", adminAuth, RawMaterialUseController.createRawMaterialUse);
router.get("/:id", RawMaterialUseController.getRawMaterialUseById);
router.put(
    "/:id",
    superadminAuth,
    RawMaterialUseController.updateRawMaterialUse
);
router.delete(
    "/:id",
    superadminAuth,
    RawMaterialUseController.deleteRawMaterialUse
);

module.exports = router;
