const express = require("express");
const UserController = require("../controllers/userController");
const { adminAuth, superadminAuth } = require("../middlewares/auth");

const router = express.Router();

// CRUD
router.get("/", UserController.getAllUser);
router.post("/", adminAuth, UserController.createUser);
router.get("/:id", UserController.getUserById);
router.put("/:id", superadminAuth, UserController.updateUser);
router.delete("/:id", superadminAuth, UserController.deleteUser);

module.exports = router;
