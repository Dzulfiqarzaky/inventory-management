const mongoose = require("mongoose");
const customerSchema = require("../schemas/customerSchema");

customerSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Customer = mongoose.model("Customer", customerSchema);

class CustomerModel {
    static async createCustomer(customerData) {
        if (customerData.name === "delete") {
            return await Customer.deleteMany();
        }
        if (customerData.name) {
            const customer = await Customer.findOne({
                name: customerData.name,
            });
            if (customer) {
                throw {
                    name: "Customer name already taken.",
                };
            }
        }

        let checkSubCustomer = false;
        if (customerData.subCustomer.length > 0) {
            const subCustomerIds = Array.isArray(customerData.subCustomer)
                ? customerData.subCustomer
                : [customerData.subCustomer];

            for (const subCustomerId of subCustomerIds) {
                const subCustomer = await Customer.findById(subCustomerId);
                if (!subCustomer) {
                    throw {
                        name: "Sub-customer Not Found",
                    };
                }
                if (subCustomer.subCustomer.length > 0) {
                    throw {
                        name: "Customer with Sub-customer cannot be added.",
                    };
                }
                if (subCustomer.isSubCustomer) {
                    checkSubCustomer = true;
                    break; // Exit the loop since we found a sub-customer
                }
            }

            if (!checkSubCustomer) {
                for (const subCustomerId of subCustomerIds) {
                    const subCustomer = await Customer.findById(subCustomerId);
                    subCustomer.isSubCustomer = true;
                    await subCustomer.save();
                }
            }
        }

        if (checkSubCustomer) {
            throw {
                name: "Sub-customer already a sub-customer of another customer.",
            };
        }

        const newCustomer = new Customer(customerData);
        const savedCustomer = await newCustomer.save();
        return savedCustomer;
    }

    static async getAllCustomer({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const customerSearchQuery = {
            $or: [
                { name: { $regex: searchText, $options: "i" } },
                { address: { $regex: searchText, $options: "i" } },
            ],
        };

        const totalData = await Customer.countDocuments(customerSearchQuery);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = {};
        sortOptions[sortBy] = sortOrder === "asc" ? 1 : -1;

        let customers;
        try {
            customers = await Customer.find(customerSearchQuery)
                .populate("subCustomer", "name")
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            customers = [];
        }

        return {
            data: customers,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getCustomerById(customerId) {
        const customer = await Customer.findById(customerId).populate(
            "subCustomer",
            "name"
        );
        if (!customer) throw { name: "Customer Not Found" };
        return customer;
    }

    static async updateCustomerById(customerId, customerData) {
        const existingCustomer = await Customer.findById(customerId);
        if (!existingCustomer) {
            throw {
                name: "Customer Not Found",
            };
        }

        // check if name already exist
        if (customerData.name) {
            const customer = await Customer.findOne({
                name: customerData.name,
            });
            if (customer && customer._id.toString() !== customerId) {
                throw {
                    name: "Customer name already taken.",
                };
            }
        }

        let checkSubCustomer = false;
        if (customerData.subCustomer.length > 0) {
            if (existingCustomer.isSubCustomer) {
                throw {
                    name: "This Customer is a Sub-customer.",
                };
            }

            const subCustomerIds = Array.isArray(customerData.subCustomer)
                ? customerData.subCustomer
                : [customerData.subCustomer];

            for (const [idx, subCustomerId] of subCustomerIds.entries()) {
                const subCustomer = await Customer.findById(subCustomerId);
                if (!subCustomer) {
                    throw {
                        name: "Sub-customer Not Found",
                    };
                }
                if (subCustomer.subCustomer.length > 0) {
                    throw {
                        name: "Customer with Sub-customer cannot be added.",
                    };
                }
                if (subCustomer.isSubCustomer) {
                    const findIdFromExistingSubCustomer =
                        existingCustomer.subCustomer.find(
                            (id) => id.toString() === subCustomerId.toString()
                        );
                    if (!findIdFromExistingSubCustomer) {
                        checkSubCustomer = true;
                        break; // Exit the loop since we found a sub-customer
                    }
                }
            }

            if (!checkSubCustomer) {
                for (const subCustomerId of subCustomerIds) {
                    const subCustomer = await Customer.findById(subCustomerId);
                    subCustomer.isSubCustomer = true;
                    await subCustomer.save();
                }

                if (
                    customerData.subCustomer.length !==
                    existingCustomer.subCustomer.length
                ) {
                    // find all exustingsubcustomer.subCustomer that not in new customerData.subcustomer
                    const subCustomerIds = existingCustomer.subCustomer.filter(
                        (id) => {
                            return !customerData.subCustomer.includes(
                                id.toString()
                            );
                        }
                    );

                    for (const subCustomerId of subCustomerIds) {
                        const subCustomer = await Customer.findById(
                            subCustomerId
                        );
                        subCustomer.isSubCustomer = false;
                        await subCustomer.save();
                    }
                }
            }
        }

        if (checkSubCustomer) {
            throw {
                name: "Sub-customer already a sub-customer of another customer.",
            };
        }

        existingCustomer.name = customerData.name;
        existingCustomer.address = customerData.address;
        existingCustomer.subCustomer = [...customerData.subCustomer];

        const updatedCustomer = await existingCustomer.save();
        return updatedCustomer;
    }

    static async deleteCustomerById(customerId) {
        const deletedCustomer = await Customer.findByIdAndDelete(customerId);
        return deletedCustomer;
    }
}

module.exports = { CustomerModel, Customer };
