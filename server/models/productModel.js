const mongoose = require("mongoose");
const productSchema = require("../schemas/productSchema");

productSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Product = mongoose.model("Product", productSchema);

class ProductModel {
    static async createProduct(productData) {
        const newProduct = new Product(productData);
        const savedProduct = await newProduct.save();
        return savedProduct;
    }

    static async getAllProduct({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const productSearchQuery = {
            $or: [
                { name: { $regex: searchText, $options: "i" } },
                { unit: { $regex: searchText, $options: "i" } },
                { qty: { $regex: searchText, $options: "i" } },
                { SKU: { $regex: searchText, $options: "i" } },
            ],
        };

        const totalData = await Product.countDocuments(productSearchQuery);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = { [sortBy]: sortOrder === "asc" ? 1 : -1 };

        let products;
        try {
            products = await Product.find(productSearchQuery)
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            products = [];
        }

        return {
            data: products,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getProductById(productId) {
        const product = await Product.findById(productId);
        if (!product) throw { name: "Product Not Found" };
        return product;
    }

    static async updateProductById(productId, productData) {
        const updatedProduct = await Product.findOneAndUpdate(
            { _id: productId },
            productData,
            { new: true }
        );
        return updatedProduct;
    }

    static async deleteProductById(productId) {
        const deletedProduct = await Product.findByIdAndDelete(productId);
        return deletedProduct;
    }
}

module.exports = { ProductModel, Product };
