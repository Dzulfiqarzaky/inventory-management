const mongoose = require("mongoose");
const rawMaterialOrderSchema = require("../schemas/rawMaterialOrderSchema");
const { RawMaterialModel, RawMaterial } = require("./rawMaterialModel");
const { Supplier } = require("./supplierModel");

rawMaterialOrderSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const RawMaterialOrder = mongoose.model(
    "RawMaterialOrder",
    rawMaterialOrderSchema
);

class RawMaterialOrderModel {
    static async createRawMaterialOrder(rawMaterialOrderData) {
        const rawMaterialToUpdate = [];
        const backupPromises = [];
        for (const [
            idx,
            rawMaterialItemData,
        ] of rawMaterialOrderData.rawMaterialItem.entries()) {
            const rawMaterialId = rawMaterialItemData.rawMaterial;
            const newQty = rawMaterialItemData.qty;

            const findRawMaterial = await RawMaterialModel.getRawMaterialById(
                rawMaterialId
            );
            const originalRawMaterial = { ...findRawMaterial.toObject() };
            backupPromises.push(originalRawMaterial);
            findRawMaterial.qty = (
                Number(newQty) + Number(findRawMaterial.qty)
            ).toString();
            rawMaterialToUpdate.push(findRawMaterial.save());
        }
        const newRawMaterialOrder = new RawMaterialOrder(rawMaterialOrderData);
        newRawMaterialOrder.totalPrice =
            rawMaterialOrderData.rawMaterialItem.reduce((acc, curr) => {
                return acc + Number(curr.price);
            }, 0);

        try {
            await Promise.all(rawMaterialToUpdate);
            const savedRawMaterialOrder = await newRawMaterialOrder.save();
            return savedRawMaterialOrder;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return RawMaterialModel.updateRawMaterialById(data._id, data);
            });
            await Promise.all(rollbackPromises);
            throw { name: "Transaction Failed." };
        }
    }

    static async getAllRawMaterialOrder({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const regexSearch = { $regex: searchText, $options: "i" };

        const rawMaterialIds = await RawMaterial.distinct("_id", {
            $or: [
                { name: regexSearch },
                { SKU: regexSearch },
                { unit: regexSearch },
            ],
        });

        const supplierIds = await Supplier.distinct("_id", {
            name: regexSearch,
        });

        const query = {
            $or: [
                { invoiceNumber: regexSearch },
                { supplier: { $in: supplierIds } },
                { "rawMaterialItem.rawMaterial": { $in: rawMaterialIds } },
            ],
        };

        const totalData = await RawMaterialOrder.countDocuments(query);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = { [sortBy]: sortOrder === "asc" ? 1 : -1 };

        let rawMaterialOrders;

        try {
            rawMaterialOrders = await RawMaterialOrder.find(query)
                .populate([
                    { path: "supplier", model: Supplier, select: "name" },
                    {
                        path: "rawMaterialItem.rawMaterial",
                        model: RawMaterial,
                        select: ["name", "SKU", "unit"],
                    },
                ])
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            console.error("Error fetching rawMaterialOrders data:", error);
            rawMaterialOrders = [];
        }

        return {
            data: rawMaterialOrders,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getRawMaterialOrderById(rawMaterialOrderId) {
        const rawMaterialOrder = await RawMaterialOrder.findById(
            rawMaterialOrderId
        ).populate([
            {
                path: "supplier",
                model: Supplier,
                select: "name",
            },
            {
                path: "rawMaterialItem.rawMaterial",
                model: RawMaterial,
                select: ["name", "SKU", "unit"],
            },
        ]);
        if (!rawMaterialOrder)
            throw { name: "Order of Raw Material Not Found" };
        return rawMaterialOrder;
    }

    static async updateRawMaterialOrderById(
        rawMaterialOrderId,
        rawMaterialOrderData
    ) {
        const rawMaterialOrder = await RawMaterialOrder.findById(
            rawMaterialOrderId
        ).populate("rawMaterialItem.rawMaterial");
        if (!rawMaterialOrder)
            throw { name: "Order of Raw Material Not Found" };

        const promises = [];
        const backupPromises = [];
        const backupRawMaterialOrderData = { ...rawMaterialOrder.toObject() };

        for (const [
            idx,
            rawMaterialItemData,
        ] of rawMaterialOrderData.rawMaterialItem.entries()) {
            const rawMaterialId = rawMaterialItemData.rawMaterial;
            const oldQty = rawMaterialOrder.rawMaterialItem[idx]?.qty ?? 0;
            const newQty = rawMaterialItemData.qty;

            const findRawMaterial = await RawMaterialModel.getRawMaterialById(
                rawMaterialId
            );
            const originalRawMaterial = { ...findRawMaterial.toObject() };
            backupPromises.push(originalRawMaterial);

            findRawMaterial.qty = (
                Number(findRawMaterial.qty) +
                (Number(newQty) - Number(oldQty))
            ).toString();
            promises.push(findRawMaterial.save());
        }

        rawMaterialOrderData.totalPrice =
            rawMaterialOrderData.rawMaterialItem.reduce((acc, curr) => {
                return acc + Number(curr.price);
            }, 0);

        try {
            await Promise.all(promises);
            const updatedRawMaterialOrder =
                await RawMaterialOrder.findOneAndUpdate(
                    { _id: rawMaterialOrderId },
                    rawMaterialOrderData,
                    { new: true }
                );
            return updatedRawMaterialOrder;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return RawMaterialModel.updateRawMaterialById(data._id, data);
            });
            await Promise.all(rollbackPromises);

            await RawMaterialOrder.findOneAndUpdate(
                { _id: rawMaterialOrderId },
                backupRawMaterialOrderData,
                { new: true }
            );
            throw { name: "Transaction Failed." };
        }
    }

    static async deleteRawMaterialOrderById(rawMaterialOrderId) {
        const deletedRawMaterialOrder =
            await RawMaterialOrder.findByIdAndDelete(rawMaterialOrderId);
        return deletedRawMaterialOrder;
    }
}

module.exports = RawMaterialOrderModel;
