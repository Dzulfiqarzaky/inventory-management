const mongoose = require("mongoose");
const rawMaterialUseSchema = require("../schemas/rawMaterialUseSchema");
const { RawMaterialModel, RawMaterial } = require("./rawMaterialModel");

rawMaterialUseSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const RawMaterialUse = mongoose.model("RawMaterialUse", rawMaterialUseSchema);

class RawMaterialUseModel {
    static async createRawMaterialUse(rawMaterialUseData) {
        for (const [
            idx,
            rawMaterialItemData,
        ] of rawMaterialUseData.rawMaterialItem.entries()) {
            const rawMaterialId = rawMaterialItemData.rawMaterial;
            const newQty = rawMaterialItemData.qty;

            const findRawMaterial = await RawMaterialModel.getRawMaterialById(
                rawMaterialId
            );

            findRawMaterial.qty = (
                Number(findRawMaterial.qty) - Number(newQty)
            ).toString();
            await findRawMaterial.save();
        }
        const newRawMaterialUse = new RawMaterialUse(rawMaterialUseData);
        const savedRawMaterialUse = await newRawMaterialUse.save();
        return savedRawMaterialUse;
    }

    static async getAllRawMaterialUse({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const rawMaterialSearchQuery = {
            $or: [
                { name: { $regex: searchText, $options: "i" } },
                { SKU: { $regex: searchText, $options: "i" } },
            ],
        };

        const rawMaterials = await RawMaterial.find(
            rawMaterialSearchQuery
        ).select("_id");

        const rawMaterialIds = rawMaterials.map(
            (rawMaterial) => rawMaterial._id
        );

        const rawMaterialUsesSearchQuery = {
            "rawMaterialItem.rawMaterial": { $in: rawMaterialIds },
        };

        const totalData = await RawMaterialUse.countDocuments(
            rawMaterialUsesSearchQuery
        );
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = { [sortBy]: sortOrder === "asc" ? 1 : -1 };

        let rawMaterialUses;

        try {
            rawMaterialUses = await RawMaterialUse.find(
                rawMaterialUsesSearchQuery
            )
                .populate({
                    path: "rawMaterialItem.rawMaterial",
                    model: RawMaterial,
                    select: ["name", "SKU", "unit"],
                })
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            console.error("Error fetching rawMaterialUses data:", error);
            rawMaterialUses = []; // If an error occurs during the query, return an empty array
        }

        return {
            data: rawMaterialUses,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getRawMaterialUseById(rawMaterialUseId) {
        const rawMaterialUse = await RawMaterialUse.findById(
            rawMaterialUseId
        ).populate({
            path: "rawMaterialItem.rawMaterial",
            model: RawMaterial,
            select: ["name", "SKU", "unit"],
        });
        if (!rawMaterialUse) throw { name: "Used Raw Material Not Found" };
        return rawMaterialUse;
    }

    static async updateRawMaterialUseById(
        rawMaterialUseId,
        rawMaterialUseData
    ) {
        const rawMaterialUse = await RawMaterialUse.findById(
            rawMaterialUseId
        ).populate("rawMaterialItem.rawMaterial");
        if (!rawMaterialUse) throw { name: "Used Raw Material Not Found" };

        for (const [
            idx,
            rawMaterialItemData,
        ] of rawMaterialUseData.rawMaterialItem.entries()) {
            const rawMaterialId = rawMaterialItemData.rawMaterial;
            const oldQty = rawMaterialUse.rawMaterialItem[idx]?.qty ?? 0;
            const newQty = rawMaterialItemData.qty;

            const findRawMaterial = await RawMaterialModel.getRawMaterialById(
                rawMaterialId
            );

            findRawMaterial.qty = (
                Number(findRawMaterial.qty) -
                (Number(newQty) - Number(oldQty))
            ).toString();
            await findRawMaterial.save();
        }
        const updatedRawMaterialUse = await RawMaterialUse.findOneAndUpdate(
            { _id: rawMaterialUseId },
            rawMaterialUseData,
            { new: true }
        );
        return updatedRawMaterialUse;
    }

    static async deleteRawMaterialUseById(rawMaterialUseId) {
        const deletedRawMaterialUse = await RawMaterialUse.findByIdAndDelete(
            rawMaterialUseId
        );
        return deletedRawMaterialUse;
    }
}

module.exports = RawMaterialUseModel;
