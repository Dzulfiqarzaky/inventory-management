const mongoose = require("mongoose");
const supplierSchema = require("../schemas/supplierSchema");

supplierSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Supplier = mongoose.model("Supplier", supplierSchema);

class SupplierModel {
    static async createSupplier(supplierData) {
        const newSupplier = new Supplier(supplierData);
        const savedSupplier = await newSupplier.save();
        return savedSupplier;
    }

    static async getAllSupplier({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const supplierSearchQuery = {
            name: { $regex: searchText, $options: "i" },
        };

        const totalData = await Supplier.countDocuments(supplierSearchQuery);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = {};
        sortOptions[sortBy] = sortOrder === "asc" ? 1 : -1;

        let suppliers;
        try {
            suppliers = await Supplier.find(supplierSearchQuery)
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            suppliers = [];
        }

        return {
            data: suppliers,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getSupplierById(supplierId) {
        const supplier = await Supplier.findById(supplierId);
        if (!supplier) throw { name: "Supplier Not Found" };
        return supplier;
    }

    static async updateSupplierById(supplierId, supplierData) {
        const updatedSupplier = await Supplier.findOneAndUpdate(
            { _id: supplierId },
            supplierData,
            { new: true }
        );
        return updatedSupplier;
    }

    static async deleteSupplierById(supplierId) {
        const deletedSupplier = await Supplier.findByIdAndDelete(supplierId);
        return deletedSupplier;
    }
}

module.exports = { SupplierModel, Supplier };
