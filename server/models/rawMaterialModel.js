const mongoose = require("mongoose");
const rawMaterialSchema = require("../schemas/rawMaterialSchema");

rawMaterialSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const RawMaterial = mongoose.model("RawMaterial", rawMaterialSchema);

class RawMaterialModel {
    static async createRawMaterial(rawMaterialData) {
        const newRawMaterial = new RawMaterial(rawMaterialData);
        const savedRawMaterial = await newRawMaterial.save();
        return savedRawMaterial;
    }

    static async getAllRawMaterial({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const rawMaterialSearchQuery = {
            $or: [
                { name: { $regex: searchText, $options: "i" } },
                { unit: { $regex: searchText, $options: "i" } },
                { qty: { $regex: searchText, $options: "i" } },
                { SKU: { $regex: searchText, $options: "i" } },
            ],
        };

        const totalData = await RawMaterial.countDocuments(
            rawMaterialSearchQuery
        );
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = {};
        sortOptions[sortBy] = sortOrder === "asc" ? 1 : -1;

        let rawMaterials;
        try {
            rawMaterials = await RawMaterial.find(rawMaterialSearchQuery)
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            rawMaterials = [];
        }

        return {
            data: rawMaterials,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getRawMaterialById(rawMaterialId) {
        const rawMaterial = await RawMaterial.findById(rawMaterialId);
        if (!rawMaterial) throw { name: "RawMaterial Not Found" };
        return rawMaterial;
    }

    static async updateRawMaterialById(rawMaterialId, rawMaterialData) {
        const updatedRawMaterial = await RawMaterial.findOneAndUpdate(
            { _id: rawMaterialId },
            rawMaterialData,
            { new: true }
        );
        return updatedRawMaterial;
    }

    static async deleteRawMaterialById(rawMaterialId) {
        const deletedRawMaterial = await RawMaterial.findByIdAndDelete(
            rawMaterialId
        );
        return deletedRawMaterial;
    }
}

module.exports = { RawMaterialModel, RawMaterial };
