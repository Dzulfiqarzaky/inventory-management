const mongoose = require("mongoose");
const billSchema = require("../schemas/billSchema");
const { ProductModel, Product } = require("./productModel");
const { Customer } = require("./customerModel");

billSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Bill = mongoose.model("Bill", billSchema);

class BillModel {
    static async createBill(billData) {
        let totalPrice = 0;
        let productsToUpdate = [];
        const backupPromises = [];

        for (const [idx, productItemsData] of billData.productItems.entries()) {
            const productId = productItemsData.product;
            const newQty = productItemsData.qty;
            totalPrice += Number(productItemsData.price);

            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);
            const updatedQty = Number(findProduct.qty) - Number(newQty);
            findProduct.qty = updatedQty.toString();
            productsToUpdate.push(findProduct);
        }

        billData.additionalCost.forEach((cost) => {
            totalPrice += Number(cost.price);
        });

        const findSubCustomer = await Customer.findById(billData.customer);

        if (findSubCustomer.subCustomer.length > 0) {
            billData.subCustomer = findSubCustomer.subCustomer.map(
                (el) => el._id
            );
        }

        const newBill = new Bill({
            ...billData,
            total: totalPrice,
        });

        const productUpdatePromises = productsToUpdate.map((product) => {
            return product.save();
        });

        try {
            await Promise.all(productUpdatePromises);

            await newBill.save();

            return newBill;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);

            throw { name: "Transaction Failed." };
        }
    }

    static async getAllBill({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const regexSearch = { $regex: searchText, $options: "i" };

        const customerIds = await Customer.distinct("_id", {
            $or: [{ name: regexSearch }, { "subCustomer.name": regexSearch }],
        });

        const query = {
            $or: [
                { billNumber: regexSearch },
                { customer: { $in: customerIds } },
            ],
        };

        const totalData = await Bill.countDocuments(query);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = { [sortBy]: sortOrder === "asc" ? 1 : -1 };
        let bills;

        try {
            bills = await Bill.find(query)
                .populate([
                    {
                        path: "productItems.product",
                        model: Product,
                        select: "name",
                    },
                    {
                        path: "customer",
                        model: Customer,
                        select: "name",
                    },
                    {
                        path: "subCustomer",
                        model: Customer,
                        select: "name",
                    },
                ])
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            console.error("Error fetching bills data:", error);
            bills = [];
        }

        return {
            data: bills,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getBillById(billId) {
        const bill = await Bill.findById(billId).populate([
            {
                path: "productItems.product",
                model: Product,
                select: "name",
            },
            {
                path: "customer",
                model: Customer,
                select: "name",
            },
            {
                path: "subCustomer",
                model: Customer,
                select: "name",
            },
        ]);
        if (!bill) throw { name: "Bill Not Found" };
        return bill;
    }

    static async updateBillById(billId, billData) {
        const bill = await Bill.findById(billId);
        if (!bill) throw { name: "Bill Not Found" };

        let totalPrice = 0;
        const promises = [];
        const backupPromises = [];
        const backupBillData = { ...bill.toObject() };

        for (const [idx, productItemsData] of billData.productItems.entries()) {
            const productId = productItemsData.product;
            const newQty = productItemsData.qty;
            const oldQty = bill.productItems[idx]?.qty ?? 0;
            totalPrice += Number(productItemsData.price);

            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);
            const updatedQty =
                Number(findProduct.qty) - (Number(newQty) - Number(oldQty));
            findProduct.qty = updatedQty.toString();

            promises.push(findProduct.save());
        }

        billData.additionalCost.forEach((cost) => {
            totalPrice += Number(cost.price);
        });

        try {
            await Promise.all(promises);
            const updatedBill = await Bill.findOneAndUpdate(
                { _id: billId },
                { ...billData, total: totalPrice },
                { new: true }
            );

            return updatedBill;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);
            await Bill.findOneAndUpdate({ _id: billId }, backupBillData, {
                new: true,
            });
            throw { name: "Transaction Failed." };
        }
    }

    static async deleteBillById(billId) {
        const deletedBill = await Bill.findByIdAndDelete(billId);
        return deletedBill;
    }
}

module.exports = BillModel;
