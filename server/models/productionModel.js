const mongoose = require("mongoose");
const productionSchema = require("../schemas/productionSchema");
const { ProductModel, Product } = require("./productModel");

productionSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Production = mongoose.model("Production", productionSchema);

class ProductionModel {
    static async createProduction(productionData) {
        const productsToUpdate = [];
        const backupPromises = [];

        for (const [
            idx,
            productItemsData,
        ] of productionData.productItems.entries()) {
            const productId = productItemsData.product;
            const newQty = productItemsData.qty;

            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);

            findProduct.qty = (
                Number(newQty) + Number(findProduct.qty)
            ).toString();
            productsToUpdate.push(findProduct);
        }
        const newProduction = new Production(productionData);
        const productUpdatePromises = productsToUpdate.map((product) => {
            return product.save();
        });
        try {
            await Promise.all(productUpdatePromises);
            const savedProduction = await newProduction.save();
            return savedProduction;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);

            throw { name: "Transaction Failed." };
        }
    }

    static async getAllProduction(searchText, page = 1, limit = 10) {
        const productSearchQuery = {
            $or: [
                { name: { $regex: searchText, $options: "i" } },
                { SKU: { $regex: searchText, $options: "i" } },
            ],
        };

        const products = await Product.find(productSearchQuery).select("_id");

        const productIds = products.map((product) => product._id);

        const productionSearchQuery = {
            "productItems.product": { $in: productIds },
        };

        const totalData = await Production.countDocuments(
            productionSearchQuery
        );
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        let production;
        try {
            production = await Production.find(productionSearchQuery)
                .populate({
                    path: "productItems.product",
                    model: Product,
                    select: ["name", "SKU"],
                })
                .skip(offset)
                .limit(limit);
        } catch (error) {
            console.error("Error fetching production data:", error);
            production = []; // If an error occurs during the query, return an empty array
        }

        return {
            data: production,
            pagination: {
                page,
                totalData,
                totalPages,
            },
        };
    }

    static async getProductionById(productionId) {
        const production = await Production.findById(productionId).populate({
            path: "productItems.product",
            model: Product,
            select: ["name", "SKU"],
        });
        if (!production) throw { name: "Production Not Found" };
        return production;
    }

    static async updateProductionById(productionId, productionData) {
        const production = await Production.findById(productionId).populate(
            "productItems.product"
        );
        if (!production) throw { name: "Production Not Found" };
        const promises = [];
        const backupPromises = [];
        const backupProductionData = { ...production.toObject() };

        for (let idx = 0; idx < productionData.productItems.length; idx++) {
            const productItemsData = productionData.productItems[idx];
            const productId = productItemsData.product;
            const oldQty = production.productItems[idx]?.qty ?? 0;
            const newQty = productItemsData.qty;
            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);

            findProduct.qty = (
                Number(findProduct.qty) +
                (Number(newQty) - Number(oldQty))
            ).toString();

            // await findProduct.save();
            promises.push(findProduct.save());
        }
        try {
            await Promise.all(promises);
            const updatedProduction = await Production.findOneAndUpdate(
                { _id: productionId },
                productionData,
                { new: true }
            );
            return updatedProduction;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);
            await Production.findOneAndUpdate(
                { _id: productionId },
                backupProductionData,
                { new: true }
            );
            throw { name: "Transaction Failed." };
        }
    }

    static async deleteProductionById(productionId) {
        const deletedProduction = await Production.findByIdAndDelete(
            productionId
        );
        return deletedProduction;
    }
}

module.exports = ProductionModel;
