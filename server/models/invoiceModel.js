const mongoose = require("mongoose");
const invoiceSchema = require("../schemas/invoiceSchema");
const { ProductModel, Product } = require("./productModel");
const { Customer } = require("./customerModel");

invoiceSchema.pre("findOneAndUpdate", function (next) {
    this.options.runValidators = true;
    next();
});

const Invoice = mongoose.model("Invoice", invoiceSchema);

class InvoiceModel {
    static async createInvoice(invoiceData) {
        let totalPrice = 0;
        let productsToUpdate = [];
        const backupPromises = [];

        for (const [
            idx,
            productItemsData,
        ] of invoiceData.productItems.entries()) {
            const productId = productItemsData.product;
            const newQty = productItemsData.qty;
            totalPrice += Number(productItemsData.price);

            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);
            const updatedQty = Number(findProduct.qty) - Number(newQty);
            findProduct.qty = updatedQty.toString();
            productsToUpdate.push(findProduct);
        }

        invoiceData.additionalCost.forEach((cost) => {
            totalPrice += Number(cost.price);
        });

        const findSubCustomer = await Customer.findById(invoiceData.customer);

        if (findSubCustomer.subCustomer.length > 0) {
            invoiceData.subCustomer = findSubCustomer.subCustomer.map(
                (el) => el._id
            );
        }

        const newInvoice = new Invoice({
            ...invoiceData,
            total: totalPrice,
        });

        const productUpdatePromises = productsToUpdate.map((product) => {
            return product.save();
        });

        try {
            await Promise.all(productUpdatePromises);

            await newInvoice.save();

            return newInvoice;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);

            throw { name: "Transaction Failed." };
        }
    }

    static async getAllInvoice({
        searchText,
        page = 1,
        limit = 10,
        sortBy = "created_at",
        sortOrder = "desc",
    }) {
        const regexSearch = { $regex: searchText, $options: "i" };

        const customerIds = await Customer.distinct("_id", {
            $or: [{ name: regexSearch }, { "subCustomer.name": regexSearch }],
        });

        const query = {
            $or: [
                { billNumber: regexSearch },
                { customer: { $in: customerIds } },
            ],
        };

        const totalData = await Invoice.countDocuments(query);
        const totalPages = Math.ceil(totalData / limit);
        const offset = limit * (page - 1);

        const sortOptions = { [sortBy]: sortOrder === "asc" ? 1 : -1 };
        let invoices;

        try {
            invoices = await Invoice.find(query)
                .populate([
                    {
                        path: "productItems.product",
                        model: Product,
                        select: "name",
                    },
                    {
                        path: "customer",
                        model: Customer,
                        select: "name",
                    },
                    {
                        path: "subCustomer",
                        model: Customer,
                        select: "name",
                    },
                ])
                .sort(sortOptions)
                .skip(offset)
                .limit(limit);
        } catch (error) {
            console.error("Error fetching invoices data:", error);
            invoices = [];
        }

        return {
            data: invoices,
            pagination: {
                page,
                totalData,
                totalPages,
                sortBy,
                sortOrder,
            },
        };
    }

    static async getInvoiceById(invoiceId) {
        const invoice = await Invoice.findById(invoiceId).populate([
            {
                path: "productItems.product",
                model: Product,
                select: "name",
            },
            {
                path: "customer",
                model: Customer,
                select: "name",
            },
            {
                path: "subCustomer",
                model: Customer,
                select: "name",
            },
        ]);
        if (!invoice) throw { name: "Invoice Not Found" };
        return invoice;
    }

    static async updateInvoiceById(invoiceId, invoiceData) {
        const invoice = await Invoice.findById(invoiceId);
        if (!invoice) throw { name: "Invoice Not Found" };

        let totalPrice = 0;
        const promises = [];
        const backupPromises = [];
        const backupInvoiceData = { ...invoice.toObject() };

        for (const [
            idx,
            productItemsData,
        ] of invoiceData.productItems.entries()) {
            const productId = productItemsData.product;
            const newQty = productItemsData.qty;
            const oldQty = invoice.productItems[idx]?.qty ?? 0;
            totalPrice += Number(productItemsData.price);

            const findProduct = await ProductModel.getProductById(productId);
            const originalProduct = { ...findProduct.toObject() };
            backupPromises.push(originalProduct);
            const updatedQty =
                Number(findProduct.qty) - (Number(newQty) - Number(oldQty));
            findProduct.qty = updatedQty.toString();

            promises.push(findProduct.save());
        }

        invoiceData.additionalCost.forEach((cost) => {
            totalPrice += Number(cost.price);
        });

        try {
            await Promise.all(promises);
            const updatedInvoice = await Invoice.findOneAndUpdate(
                { _id: invoiceId },
                { ...invoiceData, total: totalPrice },
                { new: true }
            );

            return updatedInvoice;
        } catch (error) {
            const rollbackPromises = backupPromises.map((data, idx) => {
                return ProductModel.updateProductById(data._id, data);
            });

            await Promise.all(rollbackPromises);
            await Invoice.findOneAndUpdate(
                { _id: invoiceId },
                backupInvoiceData,
                { new: true }
            );
            throw { name: "Transaction Failed." };
        }
    }

    static async deleteInvoiceById(invoiceId) {
        const deletedInvoice = await Invoice.findByIdAndDelete(invoiceId);
        return deletedInvoice;
    }
}

module.exports = InvoiceModel;
