const mongoose = require("mongoose");
const userSchema = require("../schemas/userSchema");
const { hashPassword, comparePassword } = require("../helpers/bcrypt");
const { generateToken } = require("../helpers/token");

userSchema.pre("save", async function (next) {
    if (this.isModified("password")) {
        this.password = await hashPassword(this.password);
    }
    next();
});

userSchema.pre("findOneAndUpdate", async function (next) {
    if (this._update.password) {
        this._update.password = await hashPassword(this._update.password);
    }
    this.options.runValidators = true;

    next();
});

const User = mongoose.model("User", userSchema);

class UserModel {
    static async login({ username, password }) {
        const user = await User.findOne({ username });
        if (!user || !comparePassword(password, user.password)) {
            throw { name: "Invalid Username or Password" };
        }

        const access_token = generateToken({
            username: user.username,
            role: user.role,
        });

        return {
            access_token,
            username: user.username,
            role: user.role,
        };
    }

    static async createUser(userData) {
        const newUser = new User(userData);
        const savedUser = await newUser.save();
        return savedUser;
    }

    static async getAllUser() {
        const users = await User.find();
        return users;
    }

    static async getUserById(userId) {
        const user = await User.findById(userId);
        if (!user) throw { name: "User Not Found" };
        return user;
    }

    static async updateUserById(userId, userData) {
        const updatedUser = await User.findOneAndUpdate(
            { _id: userId },
            { ...userData },
            { new: true }
        );
        return updatedUser;
    }

    static async deleteUserById(userId) {
        const deletedUser = await User.findByIdAndDelete(userId);
        return deletedUser;
    }
}

module.exports = { UserModel, User };
